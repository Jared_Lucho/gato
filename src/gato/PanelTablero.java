package gato;

import java.awt.Graphics;
import java.util.Arrays;
import javax.swing.JOptionPane;

public class PanelTablero extends javax.swing.JPanel {
    public static int tablero[][] = new int[3][3];
    private boolean jugado[][] = new boolean[3][3];
    public PanelTablero() {
        initComponents();
        int v = 100;
        for(int i=0;i<3;i++) for(int j=0;j<3;j++) tablero[i][j] = v++;
        for(int i=0;i<3;i++) for(int j=0;j<3;j++) jugado[i][j] = false;
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.drawLine(getWidth() / 3, 0, getWidth() / 3, getHeight());
        g.drawLine(getWidth() / 3 * 2, 0, getWidth() / 3 * 2, getHeight());
        g.drawLine(0, getHeight() / 3, getWidth(), getHeight() / 3);
        g.drawLine(0, getHeight() / 3 * 2, getWidth(), getHeight() / 3 * 2);
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cuarto = new javax.swing.JButton();
        tercero = new javax.swing.JButton();
        segundo = new javax.swing.JButton();
        primero = new javax.swing.JButton();
        quinto = new javax.swing.JButton();
        sexto = new javax.swing.JButton();
        septimo = new javax.swing.JButton();
        octavo = new javax.swing.JButton();
        noveno = new javax.swing.JButton();

        setBackground(new java.awt.Color(153, 153, 255));

        cuarto.setBackground(new java.awt.Color(0, 255, 204));
        cuarto.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cuarto.setMaximumSize(new java.awt.Dimension(73, 73));
        cuarto.setPreferredSize(new java.awt.Dimension(100, 73));
        cuarto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cuartoActionPerformed(evt);
            }
        });

        tercero.setBackground(new java.awt.Color(0, 255, 204));
        tercero.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        tercero.setMaximumSize(new java.awt.Dimension(73, 73));
        tercero.setPreferredSize(new java.awt.Dimension(100, 73));
        tercero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                terceroActionPerformed(evt);
            }
        });

        segundo.setBackground(new java.awt.Color(0, 255, 204));
        segundo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        segundo.setMaximumSize(new java.awt.Dimension(73, 73));
        segundo.setPreferredSize(new java.awt.Dimension(100, 73));
        segundo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                segundoActionPerformed(evt);
            }
        });

        primero.setBackground(new java.awt.Color(0, 255, 204));
        primero.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        primero.setMaximumSize(new java.awt.Dimension(73, 73));
        primero.setPreferredSize(new java.awt.Dimension(100, 73));
        primero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                primeroActionPerformed(evt);
            }
        });

        quinto.setBackground(new java.awt.Color(0, 255, 204));
        quinto.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        quinto.setMaximumSize(new java.awt.Dimension(73, 73));
        quinto.setPreferredSize(new java.awt.Dimension(100, 73));
        quinto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quintoActionPerformed(evt);
            }
        });

        sexto.setBackground(new java.awt.Color(0, 255, 204));
        sexto.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        sexto.setMaximumSize(new java.awt.Dimension(73, 73));
        sexto.setPreferredSize(new java.awt.Dimension(100, 73));
        sexto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sextoActionPerformed(evt);
            }
        });

        septimo.setBackground(new java.awt.Color(0, 255, 204));
        septimo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        septimo.setMaximumSize(new java.awt.Dimension(73, 73));
        septimo.setPreferredSize(new java.awt.Dimension(100, 73));
        septimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                septimoActionPerformed(evt);
            }
        });

        octavo.setBackground(new java.awt.Color(0, 255, 204));
        octavo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        octavo.setMaximumSize(new java.awt.Dimension(73, 73));
        octavo.setPreferredSize(new java.awt.Dimension(100, 73));
        octavo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                octavoActionPerformed(evt);
            }
        });

        noveno.setBackground(new java.awt.Color(0, 255, 204));
        noveno.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        noveno.setMaximumSize(new java.awt.Dimension(73, 73));
        noveno.setPreferredSize(new java.awt.Dimension(100, 73));
        noveno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                novenoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(primero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cuarto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(septimo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 71, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(segundo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(60, 60, 60)
                        .addComponent(tercero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(quinto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sexto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(octavo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(noveno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(36, 36, 36))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(segundo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tercero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(primero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(quinto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sexto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cuarto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(septimo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(octavo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(noveno, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void primeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_primeroActionPerformed
        if(PanelGato.jugando && !jugado[0][0]){
            jugado[0][0] = true;
            tablero[0][0] = PanelGato.turno;
            if(PanelGato.turno == 0)
                primero.setText("X");
            else primero.setText("O");
            PanelGato.turno = (PanelGato.turno + 1) % 2;
            PanelGato.txtJugador.setText("Turno: Jugador " + (PanelGato.turno + 1));
            check();
        }
    }//GEN-LAST:event_primeroActionPerformed

    private void segundoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_segundoActionPerformed
        if(PanelGato.jugando && !jugado[0][1]){
            jugado[0][1] = true;
            tablero[0][1] = PanelGato.turno;
            if(PanelGato.turno == 0)
                segundo.setText("X");
            else segundo.setText("O");
            PanelGato.turno = (PanelGato.turno + 1) % 2;
            PanelGato.txtJugador.setText("Turno: Jugador " + (PanelGato.turno + 1));
            check();
        }
    }//GEN-LAST:event_segundoActionPerformed

    private void terceroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_terceroActionPerformed
        if(PanelGato.jugando && !jugado[0][2]){
            tablero[0][2] = PanelGato.turno;
            jugado[0][2] = true;
            if(PanelGato.turno == 0)
                tercero.setText("X");
            else tercero.setText("O");
            PanelGato.turno = (PanelGato.turno + 1) % 2;
            PanelGato.txtJugador.setText("Turno: Jugador " + (PanelGato.turno + 1));
            check();
        }
    }//GEN-LAST:event_terceroActionPerformed

    private void cuartoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cuartoActionPerformed
        if(PanelGato.jugando && !jugado[1][0]){
            tablero[1][0] = PanelGato.turno;
            jugado[1][0] = true;
            if(PanelGato.turno == 0)
                cuarto.setText("X");
            else cuarto.setText("O");
            PanelGato.turno = (PanelGato.turno + 1) % 2;
            PanelGato.txtJugador.setText("Turno: Jugador " + (PanelGato.turno + 1));
            check();
        }
    }//GEN-LAST:event_cuartoActionPerformed

    private void quintoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quintoActionPerformed
        if(PanelGato.jugando && !jugado[1][1]){
            tablero[1][1] = PanelGato.turno;
            jugado[1][1] = true;
            if(PanelGato.turno == 0)
                quinto.setText("X");
            else quinto.setText("O");
            PanelGato.turno = (PanelGato.turno + 1) % 2;
            PanelGato.txtJugador.setText("Turno: Jugador " + (PanelGato.turno + 1));
            check();
        }
    }//GEN-LAST:event_quintoActionPerformed

    private void sextoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sextoActionPerformed
        if(PanelGato.jugando && !jugado[1][2]){
            tablero[1][2] = PanelGato.turno;
            jugado[1][2] = true;
            if(PanelGato.turno == 0)
                sexto.setText("X");
            else sexto.setText("O");
            PanelGato.turno = (PanelGato.turno + 1) % 2;
            PanelGato.txtJugador.setText("Turno: Jugador " + (PanelGato.turno + 1));
            check();
        }
    }//GEN-LAST:event_sextoActionPerformed

    private void septimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_septimoActionPerformed
        if(PanelGato.jugando && !jugado[2][0]){
            tablero[2][0] = PanelGato.turno;
            jugado[2][0] = true;
            if(PanelGato.turno == 0)
                septimo.setText("X");
            else septimo.setText("O");
            PanelGato.turno = (PanelGato.turno + 1) % 2;
            PanelGato.txtJugador.setText("Turno: Jugador " + (PanelGato.turno + 1));
            check();
        }
    }//GEN-LAST:event_septimoActionPerformed

    private void octavoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_octavoActionPerformed
        if(PanelGato.jugando && !jugado[2][1]){
            tablero[2][1] = PanelGato.turno;
            jugado[2][1] = true;
            if(PanelGato.turno == 0)
                octavo.setText("X");
            else octavo.setText("O");
            PanelGato.turno = (PanelGato.turno + 1) % 2;
            PanelGato.txtJugador.setText("Turno: Jugador " + (PanelGato.turno + 1));
            check();
        }
    }//GEN-LAST:event_octavoActionPerformed

    private void novenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_novenoActionPerformed
        if(PanelGato.jugando && !jugado[2][2]){
            tablero[2][2] = PanelGato.turno;
            jugado[2][2] = true;
            if(PanelGato.turno == 0)
                noveno.setText("X");
            else noveno.setText("O");
            PanelGato.turno = (PanelGato.turno + 1) % 2;
            PanelGato.txtJugador.setText("Turno: Jugador " + (PanelGato.turno + 1));
            check();
        }
    }//GEN-LAST:event_novenoActionPerformed

    public void check(){
        int turnoAnt = PanelGato.turno == 0 ? 2 : 1;
        boolean D1 = true, D2 = true;
        for(int i=0;i<2;i++){
            if(tablero[i][i] != tablero[i + 1][i + 1]) D1 = false;
            if(tablero[i][2 - i] != tablero[i + 1][2 - i - 1]) D2 = false;
        }
        if(D1 || D2){
            JOptionPane.showMessageDialog(null, "Ha ganado el jugador " + (turnoAnt));
            limpiar();
            return;
        }
        boolean lado = true;
        for(int i=0;i<3;i++){
            lado = true;
            for(int j=0;j<2;j++){
                if(tablero[i][j] != tablero[i][j + 1]) lado = false;
            }
            if(lado){
                JOptionPane.showMessageDialog(null, "Ha ganado el jugador " + (turnoAnt));
                limpiar();
                return;
            }
        }
        for(int i=0;i<3;i++){
            lado = true;
            for(int j=0;j<2;j++){
                if(tablero[j][i] != tablero[j + 1][i]) lado = false;
            }
            if(lado){
                JOptionPane.showMessageDialog(null, "Ha ganado el jugador " + (turnoAnt));
                limpiar();
                return;
            }
        }
        boolean empate = true;
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                empate &= jugado[i][j];
            }
        }
        if(empate){
            JOptionPane.showMessageDialog(null, "Los jugadores han quedado en empate.");
            limpiar();
        }
    }
    
    private void limpiar(){
        int v = 100;
        primero.setText("");
        segundo.setText("");
        tercero.setText("");
        cuarto.setText("");
        quinto.setText("");
        sexto.setText("");
        septimo.setText("");
        octavo.setText("");
        noveno.setText("");
        PanelGato.turno = 0;
        PanelGato.jugando = false;
        PanelGato.txtJugador.setText("");
        for(int i=0;i<3;i++) for(int j=0;j<3;j++) tablero[i][j] = v++;
        for(int i=0;i<3;i++) for(int j=0;j<3;j++) jugado[i][j] = false;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cuarto;
    private javax.swing.JButton noveno;
    private javax.swing.JButton octavo;
    private javax.swing.JButton primero;
    private javax.swing.JButton quinto;
    private javax.swing.JButton segundo;
    private javax.swing.JButton septimo;
    private javax.swing.JButton sexto;
    private javax.swing.JButton tercero;
    // End of variables declaration//GEN-END:variables
}
