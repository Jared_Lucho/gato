package gato;

import javax.swing.JFrame;

public class Gato {
    public static void main(String[] args) {
        JFrame f = new JFrame("Gato");
        PanelGato panel = new PanelGato();
        f.add(panel);
        f.setSize(500, 500);
        f.setLocation(100, 100);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }
    
}
